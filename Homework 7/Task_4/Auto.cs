﻿namespace Homework_7.Task_4
{
    internal class Auto
    {
        public int Number { get; set; }
        public string Brand { get; set; }
        public int Speed { get; set; }
        public int LoadCapacity { get; set; }
    }
}

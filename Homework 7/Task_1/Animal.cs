﻿namespace Homework_7.Task_1
{
    internal abstract class Animal
    {
        public abstract void MakeSound();
    }
}

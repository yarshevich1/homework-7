﻿namespace Homework_7.Task_1
{
    internal class Cat : Animal
    {
        public override void MakeSound() => Console.WriteLine("Cat say: Meow-meow");
    }
}

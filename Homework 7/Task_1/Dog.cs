﻿namespace Homework_7.Task_1
{
    internal class Dog : Animal
    {
        public override void MakeSound() => Console.WriteLine("Dog say: Bark-bark");
    }
}

﻿namespace Homework_7.Task_1
{
    internal class Lion : Animal
    {
        public override void MakeSound() => Console.WriteLine("Lion say: Roar");
    }
}

﻿namespace Homework_7.Task_1
{
    internal class PetShop
    {
        private Animal[] Animals { get; set; }   
        public PetShop(Animal[] animals)
        {
            Animals = animals;
        }

        public void MakeAllSound()
        {
            if (Animals.Length > 0)
            {
                foreach (var animal in Animals)
                {
                    animal.MakeSound();
                }
            }
        }
    }
}

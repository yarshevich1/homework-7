﻿namespace Homework_7.Task_3
{
    internal class Tram : Transport
    {
        public Tram(string destination, int number, DateTime departureTime, int numberOfSeat)
        {
            Destination = destination;
            Number = number;
            DepartureTime = departureTime;
            NumberOfSeat = numberOfSeat;
        }
        
        public override string GetTransportType() => TransportType.Rail.ToString();
    }
}

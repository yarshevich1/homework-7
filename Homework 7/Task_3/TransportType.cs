﻿namespace Homework_7.Task_3
{
    internal enum TransportType
    {
        Electric,
        Rail,
        Wheel,
        Unknown
    }
}

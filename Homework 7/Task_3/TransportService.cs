﻿namespace Homework_7.Task_3
{
    internal class TransportService
    {
        public string PrintTransportType(Transport transport) => $"Transport type: {transport.GetTransportType()}";
    }
}

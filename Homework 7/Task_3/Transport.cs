﻿namespace Homework_7.Task_3
{
    internal class Transport
    {
        public string Destination { get; set; }
        public int Number { get; set; }
        public DateTime DepartureTime { get; set; }
        public int NumberOfSeat { get; set; }

        public virtual string GetTransportType() => TransportType.Unknown.ToString();

        public sealed override string ToString() => $"Destination: {Destination}\nTransport number: {Number}\nDeparture time: {DepartureTime:HH:mm}\nNumber of seats: {NumberOfSeat}";
    }
}

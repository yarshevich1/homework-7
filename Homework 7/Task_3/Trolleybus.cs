﻿namespace Homework_7.Task_3
{
    internal class Trolleybus : Transport
    {
        public Trolleybus(string destination, int number, DateTime departureTime, int numberOfSeat)
        {
            Destination = destination;
            Number = number;
            DepartureTime = departureTime;
            NumberOfSeat = numberOfSeat;
        }
        
        public override string GetTransportType() => TransportType.Electric.ToString();
    }
}

﻿using System.Globalization;
using Homework_7.Helper;
using Homework_7.Task_1;
using Homework_7.Task_2;
using Homework_7.Task_3;

namespace Homework_7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Task 1

            Console.WriteLine("***** Task 1 *****");
            // Task 1
            PetShop petShop = new PetShop(Help.CreateNewAnimals(9));
            petShop.MakeAllSound();

            #endregion

            #region Task 2

            Console.WriteLine("***** Task 2 *****");
            // Task 2
            // Create new product's list
            List<Product> products = Help.CreateNewProducts(7);

            // Create shopping cart and add products
            ShoppingCart shoppingCart = new ShoppingCart(products);

            // Create discount strategy witch fix and percent discount 
            DiscountStrategy fixDiscountStrategy = new FixedDiscount(25);
            DiscountStrategy percentDiscountStrategy = new PercentageDiscount(15);

            // Print amount withount discount
            Console.WriteLine($"Total without discount: ${shoppingCart.TotalAmount:N}");

            // Create discount for shopping cart and
            shoppingCart.SetDiscountStrategy(fixDiscountStrategy);
            Console.WriteLine($"Total with $25 discount: ${shoppingCart.CalculateTotal():N}");

            shoppingCart.SetDiscountStrategy(percentDiscountStrategy);
            Console.WriteLine($"Total with 15% discount: ${shoppingCart.CalculateTotal():N}");

            Console.WriteLine("***** Task 3 *****");

            #endregion

            #region Task 3

            // Task 3
            // Generate new pub transport
            List<Transport> transports = Help.GetNewTransports(9);

            // Create new transport service
            TransportService transportService = new TransportService();

            // Print type transport
            transports.ForEach(t => Console.WriteLine(transportService.PrintTransportType(t)));

            // Sort transport by number of seat
            List<Transport> sortedTransport =
                transports.OrderByDescending(t => t.NumberOfSeat, Comparer<int>.Default).ToList();
            sortedTransport.ForEach(t => Console.WriteLine(t.ToString()));

            // Find transport after departure
            Console.WriteLine("Enter departure time (e.g., '08:30'): ");
            string inputTime = Console.ReadLine();
            if (DateTime.TryParseExact(inputTime, "HH:mm", null, DateTimeStyles.None,
                    out DateTime searchTime))
            {
                var afterSearchTime = transports.Where(t => t.DepartureTime > searchTime).ToList();
                Console.WriteLine($"Transport Departing After {inputTime}:");
                afterSearchTime.ForEach(t => Console.WriteLine(t.ToString()));
            }
            else
            {
                Console.WriteLine("Invalid time format.");
            }

            // Find transport
            Console.WriteLine("Enter departure time (e.g., '08:30'): ");
            string departureTime = Console.ReadLine();
            if (DateTime.TryParseExact(departureTime, "HH:mm", null, DateTimeStyles.None,
                    out DateTime searchTimeDepart))
            {
                var departSearchTime = transports.FirstOrDefault(t => t.DepartureTime == searchTimeDepart);

                if (departSearchTime != null)
                {
                    Console.WriteLine(departSearchTime.ToString());
                }
                else
                {
                    Console.WriteLine("Transport not found");
                }
            }
        }

        #endregion

            #region Task 4


            #endregion
    }
}
﻿namespace Homework_7.Task_2
{
    internal class FixedDiscount : DiscountStrategy
    {
        private double FixDiscount { get; set; }

        public FixedDiscount(double fixDiscount)
        {
            FixDiscount = fixDiscount;
        }

        public override double ApplyDiscount(double originalPrice)
        {
            if (originalPrice > FixDiscount)
                return originalPrice - FixDiscount;

            return 0.0;

        }

    }
}

﻿namespace Homework_7.Task_2
{
    internal class PercentageDiscount : DiscountStrategy
    {
        private int _percent;

        public int Percent
        {
            get => _percent;
            private set
            {
                if(value > 0 && value <= 100)
                    _percent = value;
                else
                    _percent = 0;
            }
        }

        public PercentageDiscount(int percent)
        {
            Percent = percent;
        }

        public override double ApplyDiscount(double originalPrice) => originalPrice - GetAmountDiscount(originalPrice);

        private double GetAmountDiscount(double originalPrice) => (originalPrice - _percent) / 100;
    }
}

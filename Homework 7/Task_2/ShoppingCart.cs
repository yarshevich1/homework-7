﻿namespace Homework_7.Task_2
{
    internal class ShoppingCart
    {
        private List<Product> Products { get; set; }
        private DiscountStrategy DiscountStrategy { get; set; }
        public double TotalAmount { get; private set; }

        public ShoppingCart(List<Product> products)
        {
            Products = products;
            DiscountStrategy = new DiscountStrategy();

            if (Products.Count > 0)
            {
                double totalAmount = 0;
                Products.ForEach(p => totalAmount += p.Price);
                TotalAmount = totalAmount;
            }
        }

        public void SetDiscountStrategy(DiscountStrategy discountStrategy) => DiscountStrategy = discountStrategy;

        public double CalculateTotal() => DiscountStrategy.ApplyDiscount(TotalAmount);
    }
}

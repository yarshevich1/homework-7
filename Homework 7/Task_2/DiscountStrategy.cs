﻿namespace Homework_7.Task_2
{
    internal class DiscountStrategy
    {
        public virtual double ApplyDiscount(double originalPrice) => originalPrice;
    }
}

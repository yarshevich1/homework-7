﻿using Homework_7.Task_1;
using Homework_7.Task_2;
using Homework_7.Task_3;

namespace Homework_7.Helper
{
    internal class Help
    {
        static Random rnd = new Random();

        /// <summary>
        /// Method create array with random animals
        /// </summary>
        /// <param name="size">array size</param>
        /// <returns>new array</returns>
        public static Animal[] CreateNewAnimals(int size)
        {
            Animal[] animals = new Animal[size];

            for (int i = 0; i < size; i++)
            {
                int typeAnimal = rnd.Next(3);
                switch (typeAnimal)
                {
                    case 0:
                        animals[i] = new Cat();
                        break;
                    case 1:
                        animals[i] = new Dog();
                        break;
                    case 2:
                        animals[i] = new Lion();
                        break;
                }
            }

            return animals;
        }

        /// <summary>
        /// Method ctreat lisct with random products
        /// </summary>
        /// <param name="count">List size</param>
        /// <returns>new list</returns>
        public static List<Product> CreateNewProducts(int count)
        {
            List<Product> products = new List<Product>();

            string[] possibleNames = { "Apple", "Banana", "Orange", "Milk", "Bread", "Eggs", "Cheese", "Chicken", "Potato", "Tomato" };

            for (int i = 0; i < count; i++)
            {
                string randomName = possibleNames[rnd.Next(possibleNames.Length)];
                double randomPrice = Math.Round(rnd.NextDouble() * (100.0 - 10.0) + 1.0, 2);
                products.Add(new Product(randomName, randomPrice));
            }

            return products;
        }

        /// <summary>
        /// generate random public transport
        /// </summary>
        /// <param name="count">List size</param>
        /// <returns>List public transport</returns>
        public static List<Transport> GetNewTransports(int count)
        {
            List<Transport> transports = new List<Transport>();

            string[] arrayDestination = new[]
            {
                "Central Business District", "Airport Terminal", "Beach Resort", "Shopping Mall", "Historic Downtown",
                "Amusement Park", "University Campus", "Sports Stadium", "Suburban Neighborhood",
                "Tourist Information Center"
            };

            for (int i = 0; i < count; i++)
            {
                DateTime dateTime = GetTime();
                string destination = arrayDestination[rnd.Next(arrayDestination.Length)];

               int transportType = rnd.Next(3);
               switch (transportType)
               {
                    case 0:
                        transports.Add(new Bus(destination, rnd.Next(200, 225), dateTime, rnd.Next(30, 56)));
                        break;
                    case 1:
                        transports.Add(new Trolleybus(destination, rnd.Next(200, 225), dateTime, rnd.Next(20, 51)));
                        break;
                    case 2:
                        transports.Add(new Tram(destination, rnd.Next(300, 325), dateTime, rnd.Next(20, 51)));
                        break;
                }
            }

            return transports;
        }

        /// <summary>
        /// Get random time
        /// </summary>
        /// <returns>random time</returns>
        private static DateTime GetTime()
        {
            int[] minutes = { 0, 15, 30, 45, 55, 25, 35 };
            DateTime randomTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month,
                DateTime.Now.Day, rnd.Next(7, 17), minutes[rnd.Next(minutes.Length)], 0);

            return randomTime;
        }
    }
}
